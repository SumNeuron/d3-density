# d3-density
A shameless generalization of the density plot from [d3-graph-gallery](https://www.d3-graph-gallery.com/graph/density_double.html).


# Install

```
npm i d3-density
```

# Usage

```js
import {density} from 'd3-density'
```

## Assumptions

It is assumed that the `data` bound to `density` is an object, where each density is
specified a key in the object and each value corresponds to the numeric values to estimate the density from e.g.

```js
data = {
  'A': [95,95,95,95],
  'B': [25,30,32,10],
}
```

more complex data can be provided should utility functions such as `valueExtractor`
be overwritten from their default behavior:

```js
data = {
  A: {
    x: { value: 1 },
    y: { value: 2 },
    ...
  },
  ...
}

let container = d3.select('<wherever-you-want-to-dump-the-plot>')
let d = density(container)
.valueExtractor(function(d, i){
  let subKeys = d3.keys(data[d])
  return subKeys.map((k, j)=>data[d][k].value)
})
```

## Customization
The following values are all exposed from the `density` closure:

- data
- spaceX: how much horizontal space the plot should occupy, axes exclude
- spaceY: how much vertical space the plot should occupy, axes exclude
- dataKeys: the keys from the data object, calculated for you
- dataValues: the values used to compute the densities, calculated for you
- dataExtent: the min and max of all values across datasets
- scaleX (`d3.scaleLinear()`): scale used on the x axis
- scaleY (`d3.scaleLinear()`): scale used on the y axis
- valueExtractor `()(key, index) => data[key])`: how to get the values for each key in `dataKeys` (should be a list of numeric values)
- curve (`d3.curveBasis`): the curve attribute used for rendering the densities
- colorExtractor (`(key, index) => {return d3.scaleSequential().interpolator(d3.interpolateViridis)(index / dataKeys.length)}`): how to color each ridge
- opacity (`0.5`): opacity applied to all ridges
- stroke `((k, i)=>'black')`: stroke applied to each ridge
- strokeWidth (`0.1`)
- kernel (`7`)
- kernelFineness (`40`)
- axisYTicks (`5`): number of ticks that appear on the y axis
- axisXTicks (`5`): number of ticks that appear on the x axis
- mouseover (`(k, i) => {}`): function bound to `mouseenter` __and__ `mousemove` events on each ridge
- mouseleave (`(k, i) => {}`): function bound to `mouseexit` __and__ `mouseleave` events on each ridge
- namespace (`'density'`): included in svg elements to prevent multi-instance selection issues
- axisX
- axisY
- gAxisX
- gAxisY
- areaContainer: contains the paths of the ridges
- gridlinesX (`true`)
- gridlinesY (`true`),
- applyZoom (`true`): whether or not pan and zoom should be bound to chart
- transitionDuration (`500`): applied to axes and ridges
- easeFn (`d3.easeSin`): applied to axes and ridges
- zoom `d3.zoom()`: access to the zoom instance
- zoomed (default provided): the function `zoom` invokes. Modify at your own risk.
