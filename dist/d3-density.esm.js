import { scaleLinear, curveBasis, scaleSequential, interpolateViridis, easeSin, zoom, event, keys, extent, axisBottom, axisLeft, line, mean } from 'd3';

if (typeof document !== "undefined") {
  var element = document.documentElement;
}

function kernelDensityEstimator(kernel, X) {
  return function (V) {
    return X.map(function (x) {
      return [x, mean(V, function (v) {
        return kernel(x - v);
      })];
    });
  };
}
function kernelEpanechnikov(k) {
  return function (v) {
    return Math.abs(v /= k) <= 1 ? 0.75 * (1 - v * v) / k : 0;
  };
}

var math = /*#__PURE__*/Object.freeze({
  kernelDensityEstimator: kernelDensityEstimator,
  kernelEpanechnikov: kernelEpanechnikov
});

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }
function density(container) {
  var data,
      spaceX,
      spaceY,
      dataKeys,
      dataValues,
      dataExtent,
      scaleX = scaleLinear(),
      scaleY = scaleLinear(),
      valueExtractor = function valueExtractor(key, index) {
    return data[key];
  },
      curve = curveBasis,
      colorExtractor = function colorExtractor(key, index) {
    return scaleSequential().interpolator(interpolateViridis)(index / dataKeys.length);
  },
      opacity = 0.5,
      stroke = function stroke(k, i) {
    return 'black';
  },
      strokeWidth = 0.1,
      kernel = 7,
      kernelFineness = 40,
      axisYTicks = 5,
      axisXTicks = 5,
      mouseover = function mouseover(k, i) {},
      mouseleave = function mouseleave(k, i) {},
      namespace = 'density',
      axisX,
      axisY,
      gAxisX,
      gAxisY,
      areaContainer,
      gridlinesX = true,
      gridlinesY = true,
      transitionDuration = 500,
      easeFn = easeSin,
      savedZoomState,
      applyZoom = true,
      zoom$$1 = zoom(),
      zoomed = function zoomed() {
    // container.attr("transform", d3.event.transform);
    savedZoomState = event.transform; // savedZoomState.y = 0

    areaContainer.attr("transform", savedZoomState);
    gAxisX.call(axisX.scale(savedZoomState.rescaleX(scaleX)));
    gAxisY.call(axisY.scale(savedZoomState.rescaleY(scaleY)));
  };

  density.gridlinesX = function (_) {
    return arguments.length ? (gridlinesX = _, density) : gridlinesX;
  };

  density.gridlinesY = function (_) {
    return arguments.length ? (gridlinesY = _, density) : gridlinesY;
  };

  density.transitionDuration = function (_) {
    return arguments.length ? (transitionDuration = _, density) : transitionDuration;
  };

  density.easeFn = function (_) {
    return arguments.length ? (easeFn = _, density) : easeFn;
  };

  density.savedZoomState = function (_) {
    return arguments.length ? (savedZoomState = _, density) : savedZoomState;
  };

  density.applyZoom = function (_) {
    return arguments.length ? (applyZoom = _, density) : applyZoom;
  };

  density.data = function (_) {
    return arguments.length ? (data = _, density) : data;
  };

  density.spaceX = function (_) {
    return arguments.length ? (spaceX = _, density) : spaceX;
  };

  density.spaceY = function (_) {
    return arguments.length ? (spaceY = _, density) : spaceY;
  };

  density.dataKeys = function (_) {
    return arguments.length ? (dataKeys = _, density) : dataKeys;
  };

  density.dataValues = function (_) {
    return arguments.length ? (dataValues = _, density) : dataValues;
  };

  density.dataExtent = function (_) {
    return arguments.length ? (dataExtent = _, density) : dataExtent;
  };

  density.scaleX = function (_) {
    return arguments.length ? (scaleX = _, density) : scaleX;
  };

  density.scaleY = function (_) {
    return arguments.length ? (scaleY = _, density) : scaleY;
  };

  density.valueExtractor = function (_) {
    return arguments.length ? (valueExtractor = _, density) : valueExtractor;
  };

  density.curve = function (_) {
    return arguments.length ? (curve = _, density) : curve;
  };

  density.colorExtractor = function (_) {
    return arguments.length ? (colorExtractor = _, density) : colorExtractor;
  };

  density.opacity = function (_) {
    return arguments.length ? (opacity = _, density) : opacity;
  };

  density.stroke = function (_) {
    return arguments.length ? (stroke = _, density) : stroke;
  };

  density.strokeWidth = function (_) {
    return arguments.length ? (strokeWidth = _, density) : strokeWidth;
  };

  density.kernel = function (_) {
    return arguments.length ? (kernel = _, density) : kernel;
  };

  density.kernelFineness = function (_) {
    return arguments.length ? (kernelFineness = _, density) : kernelFineness;
  };

  density.axisYTicks = function (_) {
    return arguments.length ? (axisYTicks = _, density) : axisYTicks;
  };

  density.axisXTicks = function (_) {
    return arguments.length ? (axisXTicks = _, density) : axisXTicks;
  };

  density.mouseover = function (_) {
    return arguments.length ? (mouseover = _, density) : mouseover;
  };

  density.mouseleave = function (_) {
    return arguments.length ? (mouseleave = _, density) : mouseleave;
  };

  density.axisX = function (_) {
    return arguments.length ? (axisX = _, density) : axisX;
  };

  density.axisY = function (_) {
    return arguments.length ? (axisY = _, density) : axisY;
  };

  density.gAxisX = function (_) {
    return arguments.length ? (gAxisX = _, density) : gAxisX;
  };

  density.gAxisY = function (_) {
    return arguments.length ? (gAxisY = _, density) : gAxisY;
  };

  density.areaContainer = function (_) {
    return arguments.length ? (areaContainer = _, density) : areaContainer;
  };

  density.namespace = function (_) {
    return arguments.length ? (namespace = _, density) : namespace;
  };

  density.zoom = function (_) {
    return arguments.length ? (zoom$$1 = _, density) : zoom$$1;
  };

  density.zoomed = function (_) {
    return arguments.length ? (zoomed = _, density) : zoomed;
  };

  function density() {
    dataKeys = keys(data);
    var flatValues = [];
    dataValues = dataKeys.map(function (k, i) {
      var values = valueExtractor(k, i);
      flatValues = flatValues.concat(values);
      return values;
    });
    dataExtent = extent(flatValues);

    var _dataExtent = dataExtent,
        _dataExtent2 = _slicedToArray(_dataExtent, 2),
        dataMin = _dataExtent2[0],
        dataMax = _dataExtent2[1];

    scaleX.range([0, spaceX]).domain([dataMin, dataMax]);
    var defs = container.select('defs');
    if (defs.empty()) defs = container.append('defs').attr('class', 'definitions');
    var gClip = defs.select("clipPath#".concat(namespace, "-clip-path"));
    if (gClip.empty()) gClip = defs.append('clipPath').attr('id', "".concat(namespace, "-clip-path"));
    var cpRect = gClip.select('rect');
    if (cpRect.empty()) cpRect = gClip.append('rect');
    cpRect.attr('x', 0).attr('y', 0).attr('width', spaceX).attr('height', spaceY);
    defs.raise();

    if (savedZoomState) {
      console.log(savedZoomState);
      scaleX //= savedZoomState.rescaleX(scaleX)
      .domain(savedZoomState.rescaleX(scaleX).domain()); // .range(savedZoomState.rescaleX(scaleX).range())
    } else {
      scaleX.range([0, spaceX]).domain([dataMin, dataMax]);
    }

    axisX = axisBottom(scaleX).ticks(axisXTicks);
    if (gridlinesX) axisX.tickSize(-spaceY);
    gAxisX = container.select('g.x-axis');
    if (gAxisX.empty()) gAxisX = container.append('g').attr('class', 'x-axis');
    gAxisX.transition(transitionDuration).ease(easeFn).attr('transform', "translate(".concat(0, ", ", spaceY, ")")).call(axisX);
    var ticks = scaleX.ticks(kernelFineness);
    var kde = kernelDensityEstimator(kernelEpanechnikov(kernel), ticks);
    var densities = [];
    var maxDensitiy = 0;
    dataKeys.forEach(function (k, i) {
      var density = kde(dataValues[i]);
      density.forEach(function (_ref) {
        var _ref2 = _slicedToArray(_ref, 2),
            x = _ref2[0],
            y = _ref2[1];

        if (y > maxDensitiy) maxDensitiy = y;
      });
      densities.push(density);
    });

    if (savedZoomState) {
      scaleY.domain(savedZoomState.rescaleY(scaleY).domain()).range(savedZoomState.rescaleY(scaleY).range());
    } else {
      scaleY.range([spaceY, 0]).domain([0, maxDensitiy]);
    }

    axisY = axisLeft(scaleY).ticks(axisYTicks);
    if (gridlinesY) axisY.tickSize(-spaceX);
    gAxisY = container.select('g.y-axis');
    if (gAxisY.empty()) gAxisY = container.append('g').attr('class', 'y-axis');
    gAxisY.transition(transitionDuration).ease(easeFn).call(axisY);
    var areaClipContainer = container.select('g.areas-clip');
    if (areaClipContainer.empty()) areaClipContainer = container.append('g').attr('class', 'areas-clip');
    areaContainer = areaClipContainer.select('g.areas');
    if (areaContainer.empty()) areaContainer = areaClipContainer.append('g').attr('class', 'areas');
    var areas = areaContainer.selectAll('path.density-curve').data(dataKeys);
    areas.exit().remove();
    areas = areas.merge(areas.enter().append('path').attr('class', 'density-curve'));
    areaClipContainer.attr('clip-path', "url(#".concat(namespace, "-clip-path)")); // if (savedZoomState) areaContainer.attr("transform", savedZoomState);

    var densityCurve = line().curve(curve).x(function (d) {
      return scaleX(d[0]);
    }).y(function (d) {
      return scaleY(d[1]);
    });

    var ensureFlat = function ensureFlat(d, i) {
      var min = [dataMin, 0];
      var max = [dataMax, 0];
      return [min].concat(densities[i]).concat([max]);
    };

    areas.transition(transitionDuration).ease(easeFn).attr('d', function (d, i) {
      return densityCurve(ensureFlat(d, i));
    }).attr('fill', function (k, i) {
      return colorExtractor(k, i);
    }).attr("stroke", stroke).attr("stroke-width", strokeWidth).attr('opacity', opacity);
    areas.on('mouseenter', mouseover).on('mouseover', mouseover).on('mouseleave', mouseleave).on('mouseexit', mouseleave);

    if (applyZoom) {
      zoom$$1.on('zoom', zoomed);
      container.call(zoom$$1);
    }
  }

  return density;
}

/**
* Extracts x and y of translate from transform property
* @param {string} transform transform property of svg element
* @returns {number[]} x, y of translate(x, y)
*/
function getTranslation(transform) {
  // Create a dummy g for calculation purposes only. This will never
  // be appended to the DOM and will be discarded once this function
  // returns.
  var g = document.createElementNS('http://www.w3.org/2000/svg', 'g'); // Set the transform attribute to the provided string value.

  transform = transform == undefined ? 'translate(0,0)' : transform;
  g.setAttributeNS(null, 'transform', transform); // consolidate the SVGTransformList containing all transformations
  // to a single SVGTransform of type SVG_TRANSFORM_MATRIX and get
  // its SVGMatrix.

  var matrix = g.transform.baseVal.consolidate().matrix; // As per definition values e and f are the ones for the translation.

  return [matrix.e, matrix.f];
}
function resizeDebounce(f, wait) {
  var resize = debounce(function () {
    f();
  }, wait);
  window.addEventListener('resize', resize);
}
function debounce(func, wait, immediate) {
  var timeout;
  return function () {
    var context = this,
        args = arguments;

    var later = function later() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };

    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}

var utils = /*#__PURE__*/Object.freeze({
  getTranslation: getTranslation,
  resizeDebounce: resizeDebounce,
  debounce: debounce
});

var d3_density = {
  math: math,
  density: density,
  utils: utils
};

if (typeof window !== 'undefined') {
  window.d3_density = d3_density;
}

export default d3_density;
export { density, math, utils };
