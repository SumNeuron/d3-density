import * as d3 from 'd3'
import {kernelDensityEstimator, kernelEpanechnikov} from './math'

export default function density(container) {
  let data,
  spaceX,
  spaceY,
  dataKeys,
  dataValues,
  dataExtent,
  scaleX = d3.scaleLinear(),
  scaleY = d3.scaleLinear(),
  valueExtractor = (key, index) => data[key],
  curve = d3.curveBasis,
  colorExtractor = (key, index) => {
    return d3.scaleSequential()
    .interpolator(d3.interpolateViridis)
    (index / dataKeys.length)
  },
  opacity = 0.5,
  stroke = (k, i)=>'black',
  strokeWidth = 0.1,
  kernel = 7,
  kernelFineness = 40,
  axisYTicks = 5,
  axisXTicks = 5,

  mouseover = (k, i) => {},
  mouseleave = (k, i) => {},

  namespace = 'density',

  axisX,
  axisY,
  gAxisX,
  gAxisY,
  areaContainer,

  gridlinesX = true,
  gridlinesY = true,
  transitionDuration = 500,
  easeFn = d3.easeSin,
  savedZoomState,
  applyZoom = true,
  zoom = d3.zoom(),
  zoomed = function () {
    // container.attr("transform", d3.event.transform);

    savedZoomState = d3.event.transform
    // savedZoomState.y = 0
    areaContainer.attr("transform", savedZoomState);
    gAxisX.call(axisX.scale(savedZoomState.rescaleX(scaleX)));
    gAxisY.call(axisY.scale(savedZoomState.rescaleY(scaleY)));
  }



  density.gridlinesX = function(_) { return arguments.length ? (gridlinesX = _, density) : gridlinesX; };
  density.gridlinesY = function(_) { return arguments.length ? (gridlinesY = _, density) : gridlinesY; };
  density.transitionDuration = function(_) { return arguments.length ? (transitionDuration = _, density) : transitionDuration; };
  density.easeFn = function(_) { return arguments.length ? (easeFn = _, density) : easeFn; };
  density.savedZoomState = function(_) { return arguments.length ? (savedZoomState = _, density) : savedZoomState; };
  density.applyZoom = function(_) { return arguments.length ? (applyZoom = _, density) : applyZoom; };



  density.data = function(_) { return arguments.length ? (data = _, density) : data; };
  density.spaceX = function(_) { return arguments.length ? (spaceX = _, density) : spaceX; };
  density.spaceY = function(_) { return arguments.length ? (spaceY = _, density) : spaceY; };
  density.dataKeys = function(_) { return arguments.length ? (dataKeys = _, density) : dataKeys; };
  density.dataValues = function(_) { return arguments.length ? (dataValues = _, density) : dataValues; };
  density.dataExtent = function(_) { return arguments.length ? (dataExtent = _, density) : dataExtent; };
  density.scaleX = function(_) { return arguments.length ? (scaleX = _, density) : scaleX; };
  density.scaleY = function(_) { return arguments.length ? (scaleY = _, density) : scaleY; };
  density.valueExtractor = function(_) { return arguments.length ? (valueExtractor = _, density) : valueExtractor; };
  density.curve = function(_) { return arguments.length ? (curve = _, density) : curve; };
  density.colorExtractor = function(_) { return arguments.length ? (colorExtractor = _, density) : colorExtractor; };
  density.opacity = function(_) { return arguments.length ? (opacity = _, density) : opacity; };
  density.stroke = function(_) { return arguments.length ? (stroke = _, density) : stroke; };
  density.strokeWidth = function(_) { return arguments.length ? (strokeWidth = _, density) : strokeWidth; };
  density.kernel = function(_) { return arguments.length ? (kernel = _, density) : kernel; };
  density.kernelFineness = function(_) { return arguments.length ? (kernelFineness = _, density) : kernelFineness; };
  density.axisYTicks = function(_) { return arguments.length ? (axisYTicks = _, density) : axisYTicks; };
  density.axisXTicks = function(_) { return arguments.length ? (axisXTicks = _, density) : axisXTicks; };
  density.mouseover = function(_) { return arguments.length ? (mouseover = _, density) : mouseover; };
  density.mouseleave = function(_) { return arguments.length ? (mouseleave = _, density) : mouseleave; };

  density.axisX = function(_) { return arguments.length ? (axisX = _, density) : axisX; };
  density.axisY = function(_) { return arguments.length ? (axisY = _, density) : axisY; };
  density.gAxisX = function(_) { return arguments.length ? (gAxisX = _, density) : gAxisX; };
  density.gAxisY = function(_) { return arguments.length ? (gAxisY = _, density) : gAxisY; };
  density.areaContainer = function(_) { return arguments.length ? (areaContainer = _, density) : areaContainer; };

  density.namespace = function(_) { return arguments.length ? (namespace = _, density) : namespace; };

  density.zoom = function(_) { return arguments.length ? (zoom = _, density) : zoom; };
  density.zoomed = function(_) { return arguments.length ? (zoomed = _, density) : zoomed; };





  function density() {
    dataKeys = d3.keys(data)
    let flatValues = []
    dataValues = dataKeys.map((k,i)=>{
      let values = valueExtractor(k, i)
      flatValues = flatValues.concat(values)
      return values
    })

    dataExtent = d3.extent(flatValues)
    let [dataMin, dataMax] = dataExtent

    scaleX.range([0, spaceX]).domain([dataMin, dataMax])

    let defs = container.select('defs')
    if (defs.empty()) defs = container.append('defs').attr('class', 'definitions')

    let gClip = defs.select(`clipPath#${namespace}-clip-path`)
    if (gClip.empty()) gClip = defs.append('clipPath').attr('id', `${namespace}-clip-path`)

    let cpRect = gClip.select('rect')
    if (cpRect.empty()) cpRect = gClip.append('rect')
    cpRect
    .attr('x', 0)
    .attr('y', 0)
    .attr('width', spaceX)
    .attr('height', spaceY)
    defs.raise()


    if (savedZoomState) {
      console.log(savedZoomState)
      scaleX //= savedZoomState.rescaleX(scaleX)
      .domain(savedZoomState.rescaleX(scaleX).domain())
      // .range(savedZoomState.rescaleX(scaleX).range())
    }
    else {
      scaleX.range([0, spaceX]).domain([dataMin, dataMax])
    }

    axisX = d3.axisBottom(scaleX).ticks(axisXTicks)
    if (gridlinesX) axisX.tickSize(-spaceY)
    gAxisX = container.select('g.x-axis')
    if (gAxisX.empty()) gAxisX = container.append('g').attr('class', 'x-axis')
    gAxisX
    .transition(transitionDuration)
    .ease(easeFn)
    .attr('transform',`translate(${0}, ${spaceY})`).call(axisX)




    let ticks = scaleX.ticks(kernelFineness)
    let kde = kernelDensityEstimator(kernelEpanechnikov(kernel), ticks)
    let densities = []
    let maxDensitiy = 0
    dataKeys.forEach((k, i) => {
      let density = kde(dataValues[i])
      density.forEach(([x, y])=>{
        if (y > maxDensitiy) maxDensitiy = y
      })
      densities.push(density)
    })

    if (savedZoomState) {
      scaleY
      .domain(savedZoomState.rescaleY(scaleY).domain())
      .range(savedZoomState.rescaleY(scaleY).range())
    }
    else {
      scaleY.range([spaceY ,0]).domain([0, maxDensitiy])
    }

    axisY = d3.axisLeft(scaleY).ticks(axisYTicks)
    if (gridlinesY) axisY.tickSize(-spaceX)
    gAxisY = container.select('g.y-axis')
    if (gAxisY.empty()) gAxisY = container.append('g').attr('class', 'y-axis')
    gAxisY
    .transition(transitionDuration)
    .ease(easeFn)
    .call(axisY)



    let areaClipContainer = container.select('g.areas-clip')
    if (areaClipContainer.empty()) areaClipContainer = container.append('g').attr('class', 'areas-clip')

    areaContainer = areaClipContainer.select('g.areas')
    if (areaContainer.empty()) areaContainer = areaClipContainer.append('g').attr('class', 'areas')
    let areas = areaContainer.selectAll('path.density-curve').data(dataKeys)
    areas.exit().remove()
    areas = areas.merge(areas.enter().append('path').attr('class', 'density-curve'))

    areaClipContainer.attr('clip-path', `url(#${namespace}-clip-path)`)


    // if (savedZoomState) areaContainer.attr("transform", savedZoomState);

    let densityCurve = d3.line().curve(curve)
    .x(d=>scaleX(d[0])).y(d=>scaleY(d[1]))

    let ensureFlat = (d, i)=>{
      let min = [dataMin, 0]
      let max = [dataMax, 0]
      return [min].concat(densities[i]).concat([max])
    }

    areas
    .transition(transitionDuration)
    .ease(easeFn)
    .attr('d', (d, i)=>densityCurve(ensureFlat(d, i)))
    .attr('fill', (k, i)=>colorExtractor(k, i))
    .attr("stroke", stroke)
    .attr("stroke-width", strokeWidth)
    .attr('opacity', opacity)
    areas
    .on('mouseenter',mouseover)
    .on('mouseover',mouseover)
    .on('mouseleave',mouseleave)
    .on('mouseexit',mouseleave)


    if (applyZoom) {
      zoom.on('zoom', zoomed)
      container.call(zoom)
    }

  }


  return density
}
