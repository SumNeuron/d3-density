import { event, mouse} from 'd3-selection';

import density from './modules/density'
import * as math from './modules/math'
import * as utils from './modules/utils'


const d3_density = {
  math, density, utils
}

if (typeof window !== 'undefined') {
  window.d3_density = d3_density;
}


export default d3_density
export {density, math, utils}
